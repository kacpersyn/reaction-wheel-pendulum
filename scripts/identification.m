%% experiment 1 - without adjustable mass
initial_angle = 10; %[deg]
A = deg2rad(initial_angle); %[rad]
time = 1.463; %[s]
value_at_time = 0.1062; %[rad]
lambda = -log(value_at_time/A)/time;
% plot(experiment1.Time, experiment1.Data);
f_0 = 0.681;
omega_0 = 2*pi*f_0;
damping_ratio = lambda/omega_0;
%% experiment 2 - looking for a centre of mass
l_a = 151;%[mm]
adjustable_counterweight_position_from_axis_of_rotation = l_a; %[mm]
m_a = adjustable_counterweight_length/10 * pi * (adjustable_counterweight_radius/10)^2 * adjustable_counterweight_density; %[g]
%% calculations
disc_weight = pi*disk_length*0.1*disk_density*(disk_radius*0.1)^2; %[g]
pendulum_weight = pendulum_dim(1)*pendulum_dim(2)*pendulum_dim(3)*pendulum_density*(0.1)^3; %[g]
multibody_l = 0.04514; %[m]
multibody_M = 0.618; %[kg]

combined_inertia = (l_a*m_a*((0.001)^2)*g/(omega_0^2)); %[kg*m^2]
b_e = 2*damping_ratio*sqrt(l_a*m_a*((0.001)^2)*g*combined_inertia); % (N/m)/(rad/s)