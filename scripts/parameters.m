motor_voltage = 12;
%%
M = 0.618; %[kg] TBD
g = 9.81; % m/s^2
l = 0.0452; %[m] TBD
I_0 = 0.015; %[kg*m^2] TBD
bx1 = 0.0102; % pendulum axis friction coef [TBD - N*m/(rad/s)?] TBD
k = 0.027; % back-emf constant [V/(rad/s)] TBD
R = 2.4; %[Ohm] TBD
I_r = 1.1643e-04; %rotor inertia [kg*m^2] TBD
b_e = 0.001; % rotor friction coef[TBD - N*m/(rad/s)?] TBD

initial_pendulum_angle = 3.03;