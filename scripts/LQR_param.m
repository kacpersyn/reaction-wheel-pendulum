x1_e = pi
x2_e = 0
x3_e = 0
%%
a23 = (b_e/I_0 + 2*k^2/(I_0*R));
a33 = -(b_e/I_0 + 2*k^2/(I_0*R) + k^2/(I_r*R) + b_e/I_r)


A = [
    0,                 1,      0;
    -M*g*l/I_0*cos(x1_e),-bx1/I_0, a23;
    M*g*l/I_0*cos(x1_e), bx1/I_0, a33;
    ]

B = [
    0;
    -2*k/(I_0*R);
    (2*k/(I_0*R) + k/(I_r*R))
    ]

q_1 = 1/0.2^2
q_2 = q_1;
q_3 = q_1;
R_lqr = 100;

Q = diag([q_1, q_2, q_3])


[K, S, e] = lqr(A, B, Q, R_lqr)